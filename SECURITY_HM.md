
# Lab 9

## Resource
https://text.ru

## №1 Authentication

### Source
https://cheatsheetseries.owasp.org/cheatsheets/Authentication_Cheat_Sheet.html#user-ids

We have to make sure that emails are case-insensitive and unique.

| Steps                             | Results                                          |   |   |   |
|-----------------------------------|--------------------------------------------------|---|---|---|
| Go to the https://text.ru page    | Ok                                               |   |   |   |
| Click register button             | Ok                                               |   |   |   |
| Write miyim47616@soombo.com email | Ok                                               |   |   |   |
| Proove that you are not a robot   | Ok                                               |   |   |   |
| Agree with Policy                 | OK                                               |   |   |   |
| Click register                    | Ok                                               |   |   |   |
| Check recieved email              | Not ok, got plain generated password             |   |   |   |
| Go to the register page again     | Ok                                               |   |   |   |
| Write mIYIm47616@soOmbo.com       | Ok, got the message that this mail already exist |   |   |   |

### Results

Now we can check which OWASP requirements the website follows.

| Requirement | Result |
| - | - |
| Emails should be case-insensitive | + |

However we got the generated password in plain format. This is not secure. Account enumeration attack is possible.


## №2: Forgot Password Check

Assume that we already have an account (lets just use the email for previous test case)

| Steps                             | Results                                    |   |   |   |
|-----------------------------------|--------------------------------------------|---|---|---|
| Go to the https://text.ru page    | Ok                                         |   |   |   |
| Click **Войти** button            | Ok                                         |   |   |   |
| Click forgot password button      | Ok                                         |   |   |   |
| Write miyim47616@soombo.com email | Ok                                         |   |   |   |
| Proove that you are not a robot   | Ok                                         |   |   |   |
| Click **Восстановить** button     | OK                                         |   |   |   |
| Check recieved email              | Ok, got the link for accessing to the site |   |   |   |
| Click to the url from this email  | Ok, got access for account                 |   |   |   |


### Results
Got special url, not a new or old plain password. Thats good and secure.


## №3: Profile photo upload


| Steps                          | Results                                         |   |   |   |
|--------------------------------|-------------------------------------------------|---|---|---|
| Go to the https://text.ru page | Ok                                              |   |   |   |
| Enter the account              | Ok                                              |   |   |   |
| Click to the profile photo     | Ok                                              |   |   |   |
| Click edit button              | Ok                                              |   |   |   |
| Add another profile photo      | Ok                                              |   |   |   |
| Click save button              | OK                                              |   |   |   |
| Check that new photo was added | Ok                                              |   |   |   |
| Try upload huge photo          | Got error "Максимальный размер изображения 2MB" |   |   |   |

### Results
No problems was found

